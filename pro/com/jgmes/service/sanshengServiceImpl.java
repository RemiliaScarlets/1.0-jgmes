package com.jgmes.service;

import com.je.core.security.SecurityUserHolder;
import com.je.core.service.PCDynaServiceTemplate;
import com.je.core.service.PCServiceTemplate;
import com.je.core.util.JEUUID;
import com.je.core.util.bean.BeanUtils;
import com.je.core.util.bean.DynaBean;
import com.je.rbac.model.Department;
import com.je.rbac.model.EndUser;
import com.je.rbac.service.UserManager;
import com.jgmes.util.JgmesCommon;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * ��ʤ����Service
 * @author admin
 * @version 2019-11-13 20:53:43
 */
@Component("sanshengService")
public class sanshengServiceImpl implements sanshengService  {

	/**��̬Bean(DynaBean)�ķ����*/
	private PCDynaServiceTemplate serviceTemplate;
	/**ʵ��Bean���������,��Ҫ����SQL*/
	private PCServiceTemplate pcServiceTemplate;
	/**�û������*/
	private UserManager userManager;
	
	public void load(){
		System.out.println("hello serviceimpl");
	}

	/**
	* Description: 仅用于三胜物料资料导入
	* @Param:  * @param null
	* @return:
	* @author: ljs
	* @date: 2019/11/15 15:57
	*/
	@Override
	public DynaBean importProductData(DynaBean order) {
		Boolean haveError = false;
		StringBuilder errorMessage=new StringBuilder(2000);//错误信息记录
		JgmesCommon jgmesCommon = new JgmesCommon(null, serviceTemplate);
		if (order!=null){
			//校验该产品编号是否已经存在
			String productdata_bh = order.getStr("PRODUCTDATA_BH");
			List<DynaBean> jgmes_base_productdata = serviceTemplate.selectList("JGMES_BASE_PRODUCTDATA", "and PRODUCTDATA_BH='" + productdata_bh + "'");
			if (jgmes_base_productdata.size()>0) {
				haveError = true;
				errorMessage.append("该产品编号已存在");
			}
			//校验该产品名称是否存在
			String productdata_name = order.getStr("PRODUCTDATA_NAME");
			List<DynaBean> jgmes_base_productdata1 = serviceTemplate.selectList("JGMES_BASE_PRODUCTDATA", "and PRODUCTDATA_NAME='" + productdata_name + "'");
			if (jgmes_base_productdata1.size()>0) {
				haveError = true;
				errorMessage.append("该产品名称已存在");
			}
			//校验物料属性
			String productdata_wltype_code = order.getStr("PRODUCTDATA_WLTYPE_CODE");
			DynaBean jgmes_wltype = jgmesCommon.getDic("JGMES_WLTYPE", productdata_wltype_code);
			if (jgmes_wltype==null){
				haveError = true;
				errorMessage.append("该物料属性不存在");
			}else{
				order.set("PRODUCTDATA_WLTYPE_CODE",jgmes_wltype.get("DICTIONARYITEM_ITEMCODE"));
				order.set("PRODUCTDATA_WLTYPE_NAME",jgmes_wltype.get("DICTIONARYITEM_ITEMNAME"));
			}
			//校验工艺路线
			String productdata_cpgylx = order.getStr("PRODUCTDATA_CPGYLX");
			DynaBean jgmes_gygl_gylx = serviceTemplate.selectOne("JGMES_GYGL_GYLX", "and GYLX_GYLXNUM='" + productdata_cpgylx + "' and GYLX_STATUS=1");
			if (jgmes_gygl_gylx==null){
				haveError = true;
				errorMessage.append("该工艺路线不存在或未被启用");
			}else{
				order.set("PRODUCTDATA_CPGYLX",jgmes_gygl_gylx.get("GYLX_GYLXNAME"));
				order.set("PRODUCTDATA_CPGYLXID",jgmes_gygl_gylx.get("GYLX_ID"));
			}
			order.set("PRODUCTDATA_STATUS_CODE",1);
			order.set("PRODUCTDATA_STATUS_NAME","启用");
			String pk = JEUUID.uuid();
			order.set(BeanUtils.KEY_PK_CODE,pk);

			if (haveError) {
				order.set("error", "序号:" + order.getStr("rownumberer_1") + "的错误信息为：" + errorMessage.toString());
			}else {
				DynaBean jgmes_sys_tmbz = serviceTemplate.selectOne("JGMES_SYS_TMBZ", "and TMBZ_TMFL_CODE='CPM'");
				if (jgmes_sys_tmbz!=null){
					//子表自动写入
					DynaBean print = new DynaBean();
					print.set(BeanUtils.KEY_TABLE_CODE, "JGMES_BASE_CPTMYYGG");
					print.set("CPTMYYGG_CPBH",productdata_bh);//产品编号
					String uuid = JEUUID.uuid();
					print.set("CPTMYYGG_YYGZBH",uuid);
					print.set("CPTMYYGG_YYGGMC","产品条码规则");//应用规则名称
					print.set("CPTMYYGG_TMXZ_CODE","TMXZX02");//条码性质，唯一码
					print.set("CPTMYYGG_TMXZ_NAME","唯一码");//条码性质，唯一码
					print.set("CPTMYYGG_TMLX_CODE","TMLX01");//条码类型
					print.set("CPTMYYGG_TMLX_NAME","产品条码");//条码类型
					print.set("CPTMYYGG_BQMB",jgmes_sys_tmbz.get("TMBZ_BQMBWJ"));//模板文件
					print.set("CPTMYYGG_BQCS",jgmes_sys_tmbz.get("TMBZ_BQCSMB"));//模板参数
					print.set("CPTMYYGG_MTMSL",1);//每条码用量
					print.set("CPTMYYGG_BARCODEMODEL_CODE","BARCODEMODEL01");//条码生成方式
					print.set("CPTMYYGG_TMSCLX_CODE","TMSCLX03");//条码生成类型，按照工单生成
					print.set("CPTMYYGG_TMSCLX_NAME","按工单生成");//条码生成类型，按照工单生成
					print.set("CPTMYYGG_TMGZBH",jgmes_sys_tmbz.get("TMBZ_YZZZ"));//条码规则编号，取验证正则字段
					print.set("CPTMYYGG_STATUS_CODE",1);//状态
					print.set("CPTMYYGG_STATUS_NAME","启用");//状态
					print.set("JGMES_BASE_CPTMYYGG_ID",uuid);
					print.set(BeanUtils.KEY_PK_CODE,uuid);
					print.set("JGMES_BASE_PRODUCTDATA_ID","");
					serviceTemplate.insert(print);
				}else{
					order.set("error", "序号:" + order.getStr("rownumberer_1") + "的错误信息为：条码标识中没有设定产品码规则");
				}
			}
		}
		return order;
	}


	/**
	 * ��ȡ��¼�û�
	 * @return
	 */
	public EndUser getCurrentUser() {
		// TODO Auto-generated method stub
		return SecurityUserHolder.getCurrentUser();
	}
	/**
	 * ��ȡ��¼�û����ڲ���
	 * @return
	 */
	public Department getCurrentDept() {
		// TODO Auto-generated method stub
		return SecurityUserHolder.getCurrentUserDept();
	}
	@Resource(name="PCDynaServiceTemplate")
	public void setServiceTemplate(PCDynaServiceTemplate serviceTemplate) {
		this.serviceTemplate = serviceTemplate;
	}
	@Resource(name="PCServiceTemplateImpl")
	public void setPcServiceTemplate(PCServiceTemplate pcServiceTemplate) {
		this.pcServiceTemplate = pcServiceTemplate;
	}
	@Resource(name="userManager")
	public void setUserManager(UserManager userManager) {
		this.userManager = userManager;
	}
}