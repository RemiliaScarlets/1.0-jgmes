package com.jgmes.action;

import com.je.core.action.DynaAction;
import com.jgmes.service.FinalQualityAssureanceServices;
import com.jgmes.service.FinalQualityAssureanceServicesImpl;
import com.jgmes.util.JgmesResult;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * FQA Action
 *
 * @author ljs
 * @version 2019-06-04 11:58:00
 * @see /jgmes/finalQualityAssureanceAction!load.action
 */
@Component("finalQualityAssureanceAction")
@Scope("prototype")
public class FinalQualityAssureanceAction extends DynaAction {

    private static final long serialVersionUID = 1L;

    public void load() {
        toWrite("hello Action");
    }

    FinalQualityAssureanceServices fqaServices = new FinalQualityAssureanceServicesImpl();

    public void getBarcodeData() {
        String barCode = request.getParameter("Barcode");//SN码
        String OuterBarCode = request.getParameter("OuterBarCode");//外箱码
        String powerLineCode = request.getParameter("powerLineCode");//电源线码
        String BoxCode = request.getParameter("BoxCode");//附件码
        String ssh = request.getParameter("ssh");
        JgmesResult<List<HashMap>> ret = fqaServices.getBarcodeData(barCode, OuterBarCode, powerLineCode, BoxCode, ssh);
        toWrite(jsonBuilder.toJson(ret));
    }

    public void saveFQAData() {
        String barCode = request.getParameter("Barcode");//SN码
        String OuterBarCode = request.getParameter("OuterBarCode");//外箱码
        String powerLineCode = request.getParameter("powerLineCode");//电源线码
        String BoxCode = request.getParameter("BoxCode");//附件码
        JgmesResult<List<HashMap>> ret = fqaServices.saveFQAData(barCode, OuterBarCode, powerLineCode, BoxCode);
        toWrite(jsonBuilder.toJson(ret));
    }

    public void getSNBarCodeData() {
        String barCode = request.getParameter("Barcode");//SN码
        String ssh = request.getParameter("ssh");     //工序顺序号
        JgmesResult<List<HashMap>> ret = fqaServices.getSNBarcodeData(barCode, ssh);
        toWrite(jsonBuilder.toJson(ret));
    }

    public void save() {
        String jsonStr = request.getParameter("jsonStr");//主表Json
        String jsonStrDetail = request.getParameter("jsonStrDetail");     //从表Json
        JgmesResult<List<HashMap>> ret = fqaServices.save(jsonStr, jsonStrDetail, request);
        toWrite(jsonBuilder.toJson(ret));
    }

}