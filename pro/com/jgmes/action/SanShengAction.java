package com.jgmes.action;

import com.je.core.action.DynaAction;
import com.je.core.util.StringUtil;
import com.je.core.util.bean.DynaBean;
import com.jgmes.util.JgmesResult;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;

/**
 * ��ʤ������
 * @author admin
 * @version 2019-11-13 20:45:29
 * @see /jgmes/sanShengAction!load.action
 */
@Component("sanShengAction")
@Scope("prototype")
public class SanShengAction extends DynaAction  {
	
	private static final long serialVersionUID = 1L;
	
	public void load(){
		toWrite("hello Action");
	}

	/**
	* Description: 根据条件获取当前的生产任务列表
	* @Param:  * @param null
	* @return:
	* @author: ljs
	* @date: 2019/11/13 20:51
	*/
	public void getProductionTaskData(){
		JgmesResult<List<HashMap>> ret = new JgmesResult<>();
		String pageSize = request.getParameter("pageSize");
		String currPage = request.getParameter("currPage");
		String pcDate = request.getParameter("pcDate");
		String cpKeyWord = request.getParameter("cpKeyWord");
		String DH = request.getParameter("DH");
		StringBuilder selectSql = new StringBuilder();
		//排产日期
		if (StringUtil.isNotEmpty(pcDate)) {
			pcDate = pcDate.replaceAll("/", "-");
			selectSql.append(" and SCRW_PCRQ='" + pcDate + "'");
		}
		//产品信息
		if (StringUtil.isNotEmpty(cpKeyWord)) {
			selectSql.append(" and (SCRW_CPBH like '%" + cpKeyWord + "%' or SCRW_NAME like '%" + cpKeyWord + "%' or SCRW_CPGG like '%" + cpKeyWord + "%') ");
		}
		//单据信息
		if (StringUtil.isNotEmpty(DH)) {
			selectSql.append(" and (SCRW_DDHM like '%" + DH + "%' or SCRW_RWDH like '%" + DH + "%') ");
		}

		List<DynaBean> jgmes_plan_scrw = serviceTemplate.selectList("JGMES_PLAN_SCRW", selectSql.toString());
		ret.TotalCount = Long.valueOf(jgmes_plan_scrw.size());
		if (pageSize != null && !"".equals(pageSize) && currPage != null && !"".equals(currPage)) {
			int start = Integer.parseInt(pageSize) * (Integer.parseInt(currPage) - 1);
			int size = Integer.parseInt(pageSize);
			selectSql.append("  LIMIT " + start + "," + size + "");
			List<DynaBean> jgmes_pb_bgsj1 = serviceTemplate.selectList("JGMES_PLAN_SCRW", selectSql.toString());
			ret.Data = ret.getValues(jgmes_pb_bgsj1);
		}
		toWrite(jsonBuilder.toJson(ret));
	}
	
}